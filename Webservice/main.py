import uvicorn 
import logging 
from setup.api import API


def main():


    """
    Start de the web service
    """
    Web_service=API()
    uvicorn.run(Web_service.app)


if __name__ == "__main__":
    main()
