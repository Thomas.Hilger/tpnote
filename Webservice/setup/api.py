from fastapi import FastAPI
import random
import requests
import os 
import sys





sys.path.insert(1, str(os.getcwd())+'/setup')
from core import *
class API():

    """
    a class creating an API with the pacakge fastapi
    
    """

    def __init__(self):
        self.app=FastAPI(
        title="MusicForRuddy",
        description="Une petite application pour aider Ruddy pour sa soirée karaokée",
        version="1.0.0")
        


        @self.app.get("/")
        async def api_status():
            return {"Server check":"200"}



 

        @self.app.get("/random/{artist_name}")
        async def randomsonglyrics(artist_name):

            songs=get_all_songs(artist_name)
            try:
                title=random.choice(songs)
                indice=songs.index(title)
                youtube=get_all_youtube(artist_name)[indice]

            except:
                title=None
                youtube=None

            lyric=get_lyrics(artist_name,title)
            
            

            return {"artiste":artist_name,"title":title,"suggested_youtube_url":youtube,"lyrics":lyric}

