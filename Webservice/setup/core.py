import requests
import os
from dotenv import load_dotenv

load_dotenv()

def get_artistid(artiste):
    """
    returns the id of an artiste  using audioDB API

    parameters : 

            artiste : str

    Returns :

            artisteId : integer or None if the artist wasn't found
    
    """

    
    try :
        requete=requests.get(os.getenv("api_audio_artist")+artiste)
        return requete.json()['artists'][0]['idArtist']

    except:
        return None 


def get_albums(artiste):

    """
    returns all the albums' id of an artiste using audioDB API

    parameters : 

            artiste : str

    Returns :

            id_albums : list of integer or None if no album found
    
    """
    try:
        id_artiste=get_artistid(artiste)
        requete=requests.get(os.getenv("api_audio_album")+str(id_artiste))
        n= len(requete.json()['album'])
        id_albums=[]
        for i in range(n):
            id_albums.append(requete.json()['album'][i]['idAlbum'])


        return id_albums

    except:
        return None



def get_all_songs(artiste):

    """
    returns all the songs of an artist using audioDB API

    parameters : 

            artiste : str

    Returns :

            songs : list of string, each string is the title of a song made by the artist
    
    """
    id_albums=get_albums(artiste)
    songs=[]
    try :
        for album in id_albums:
            requete=requests.get(os.getenv("api_audio_song")+str(album))
            n=len(requete.json()["track"])
            for i in range(n):
                try :
                    songs.append(requete.json()['track'][i]['strTrack'])
                
                except :
                    pass
                
        return songs 

    except : 

        return None

def get_all_youtube(artiste):

    """
    returns all the url adress of the clip for every song made by an artist (if it exists) using audioDB API

    parameters : 

            artiste : str

    Returns :

            youtubes : list of string, each string is a url adress or null if it doesn't exist
    
    """
    id_albums=get_albums(artiste)
    youtubes=[]

    for album in id_albums:
        requete=requests.get(os.getenv("api_audio_song")+str(album))
        n=len(requete.json()["track"])
        for i in range(n):
            try :
                 youtubes.append(requete.json()['track'][i]['strMusicVid'])
            
            except :
                pass



    return youtubes


def get_lyrics(artist_name,title):

    """
    returns the lyric of the title made by an artist using the yricsovh API if it exists

    parameters : 

            artiste_name : str
            title : str

    Returns :

            lyrics : string or None if no lyrics found
    
    """
    try:

        requete=requests.get(os.getenv("api_lyric")+"{}/{}".format(artist_name,title))
        
        return requete.json()['lyrics']
    except :

        return None  


