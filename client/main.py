import requests
import json
from dotenv import load_dotenv
import os 



def load_params_from_json(file_json):
    """
    a function that charged a json file

    parameters :
            file_json: str the name of the json file

    Returns : 

            file : list of dict 
    """
    with open(file_json) as f:
        return json.load(f)

        

def create_playlist(file_name):
    """
    create a playlist with the taste written in the json file

    parameters : 
            file_name : str the name of the json file

    returns : 
            playlist : list of dict with 4 keys (artiste_name, title, suggested_youtube_url, lyrics)
    
    
    """
    rudy=sorted(load_params_from_json(file_name), key=lambda d: d['note'],reverse=True) 
    playlist=[]
    while len(playlist)<20:

        for artiste in rudy:
                if len(playlist)<20:
                        requete=requests.get(os.getenv("url_server")+"/random/"+artiste['artiste']).json()
                        playlist.append(requete)


                
        with open('playlist.json', 'w') as mon_fichier:
                json.dump(playlist, mon_fichier)
                
        return playlist




if __name__ == "__main__":
    load_dotenv()
    print(create_playlist(os.getenv("fichier_json")))
 




