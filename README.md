
# Playlist pour la soirée de Rudy

Pour endiabler vos soirées, voici une magnifique applications pour vous générer une playlist de chansons suivant vos goûts ! Pour se faire, nous utilisons deux API musicales, à savoir AudioDB (https://www.theaudiodb.com/api_guide.php) et Lyrics OVH (https://lyricsovh.docs.apiary.io/).

## Architecture du projet 

```mermaid
graph TD
    A[Client Python] --> |HTTP| B[Webservice Python]
    B --> |HTTP| C[API AudioDB]
    C --> |JSON| B
    B --> |HTTP| D[API Lyrics OVH]
    D --> |JSON| B
    B --> |JSON| A
``` 

schéma du répertoire : 

```
tpnote
└───Webservice
|   └───setup
|   |   --fichiers serveur
|   |
|   |--main
|
└───client
|   |--fichier json pour l'exemple de scénario
|   |
|   |--main
└───test
    └─── WebserviceTest
    |   -- fichier pour les tests unitaires
```


## Installer le projet

Cloner le dépôt : 
```
git clone https://gitlab.com/Thomas.Hilger/tpnote.git
cd tpnote
pip install -r requirements.txt

```


## Lancer le Webservice 

Pour lancer le Webservice, exectuez dans le terminal à la racine du projet  :
```
cd Webservice
python main.py

```
Pour vérifier l'état de fonctionnement du Webservice :
```
http://localhost:8000/
```

Pour obtenir aléatoirement une musique d'un artiste :

```
http://localhost:8000/random/{artiste_name}
```

##  Lancer les tests unitaires (optionnel)

Pour s'asurer que l'application fonctionne correctement vous pouvez  executer les tests unitaires. Exectuez dans le terminal à la racine du projet :
``` 
pytest

```

Si tout se passe bien, 3 tests devraient être validés avec succès.

## Exemple de scénario client pour avoir votre playlist
Rudy souhaite créer une playlist de 20 chansons composée de ses artistes préférés. Il a donc préparé un fichier JSON contenant le nom de ses artistes. Il se trouve dans le chemin `/Client/rudy.json`. 

Il commence par lancer le Webservice (cf partie précédente).
Maintenant que le webservice est disponible, il peut créer sa playlist ! Exectuez dans le terminal à la racine du projet :

```
cd client
python main.py
```

Et voilà ! Au bout de quelques secondes (plutôt deux minutes..) la playlist est générée avec les paroles de 20 chansons de ses artistes préférés dans un fichier json dans le répertoire client. 





